CC=gcc
CFLAGS=-std=c99 -Wall


TESTFLAGS=-fprofile-arcs -ftest-coverage

clean:
	rm -f *.o testhasard testcomparison testapp *.gcov *.gcda *.gcno
testhasard: testhasard.c hasard.h hasard.c
	# the hasard test
	$(CC) $(CFLAGS) $(TESTFLAGS) -c testhasard.c hasard.c
	$(CC) $(CFLAGS) $(TESTFLAGS) -o testhasard testhasard.o hasard.o
	# run the hasard test
	./testhasard
	gcov -cp testhasard
